<?php
//サイト共通の情報を設定
$fe_protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$fe_domainName = $_SERVER['HTTP_HOST'];
$fe_site_uri = $fe_protocol . $fe_domainName;
$fe_site_path = htmlspecialchars($_SERVER["REQUEST_URI"], ENT_QUOTES, 'UTF-8');
$fe_page_url = $fe_site_uri.$fe_site_path;

$fe_site_name = '○○○○ショッピングセンター';
$fe_site_fb_appId = ''; //不明な場合は値を空にしてください
$fe_site_default_ogpimage = $fe_site_uri . '/assets/images/ogp.png';
?>
