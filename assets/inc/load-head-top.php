<?php /* アナリティクスの記述などはこのファイルに記述 */ ?>

<?php /* ?>
<?php if ($preview_mode) { ?>
<meta name="robots" content="noindex,nofollow">
<?php } ?>
<?php if (!$preview_mode) { ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXXXXX-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-XXXXXXXXX-1');
</script>
<?php } ?>
<?php */ ?>
