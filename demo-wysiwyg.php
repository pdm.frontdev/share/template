<?php /*

///////////// サンプルの使用方法 /////////////

ショップニュース、ショップブログなど管理画面の入力欄にwysiwygを使用するコンテンツに関して使用してください。
後述のdiv.wysiwygごとそのままhtmlソースとして使用してください。
また、wysiwyg.cssに関しては、中身のソースだけをコピーし、common.cssやlayout.cssなどのグローバルなcssファイルに記述するようにしてください。
リンクのフォントカラー等の追加は可能です。


///////////// wysiwyg内のtableに関して /////////////

tableに関してはスマホ閲覧時には画面をはみ出し、横スクロールをさせる想定です。
横スクロールさせる際にtableをdivでラップさせる為jsを記述しています。
このサンプルではファイル内の下部に記載していますが、実際にはcommon.jsなどのグローバルなJSファイルの中に記述してください。
また、このサンプルのJSソースはあくまでサンプルとしています。
div.wysiwyg内のtableをdiv.table-wrapでラップすることができれば変更していただいて構いません。

*/ ?>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/config.php';

//ページ設定
$fe_page_title = 'WYSIWYGのデモ | '.$site_name;
$fe_page_description = '＜本文先頭の100文字を出力＞';
$fe_page_keywords = ''; //不明な場合は値を空にしてください
$fe_page_ogpimage = '＜記事のサムネールの同じ画像のパスを出力＞'; //OGP画像URL
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/meta.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-head-top.php';?>

<?php /* stylesheet */ ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/common-css.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/common-head-js.php';?>
<link rel="stylesheet" href="/assets/css/demo-wysiwyg.css">
<link rel="stylesheet" href="/assets/css/wysiwyg.css">
<script type="application/ld+json">
</script>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-head-bottom.php';?>
</head>
<body>
    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-body-top.php';?>

    <div class="wrapper">

        <?php /* ----------------- WYSIWYG用のラッパー ----------------- */ ?>
        <div class="wysiwyg">

            <p><span style="font-size: 140%;">フォントサイズ 140%</span><br /><span style="font-size: 120%;">フォントサイズ 120%</span><br />フォントサイズ 100%<br /><span style="font-size: 80%;">フォントサイズ 80%</span><br /><span style="font-size: 60%;">フォントサイズ 60%</span></p>
            <p>&nbsp;</p>
            <p>色<br /><span style="color: #ff0000;">テキストカラー 赤</span><br /><span style="color: #0000ff;">テキストカラー 青</span></p>
            <p>&nbsp;</p>
            <p><strong>太字</strong><br /><span style="font-size: 120%;"><strong>太字＋フォントサイズ 120%</strong></span><br /><br /></p>
            <p><em>イタリック</em><br /><span style="font-size: 80%;"><em>イタリック＋フォントサイズ 80%</em></span></p>
            <p>&nbsp;</p>
            <p><span style="text-decoration: underline;">下線</span></p>
            <p>&nbsp;</p>
            <p><span style="text-decoration: line-through;">取り消し線</span></p>
            <p>&nbsp;</p>
            <p>上付文字<sup>2</sup></p>
            <p>&nbsp;</p>
            <p>下付文字<sub>1</sub></p>
            <p>&nbsp;</p>
            <p>コード</p>
            <code><p class="hoge">コードが入ります</p></code>
            <p>&nbsp;</p>
            <p>ブロック</p>
            <h1>見出し1</h1>
            <h2>見出し2</h2>
            <h3>見出し3</h3>
            <h4>見出し4</h4>
            <h5>見出し5</h5>
            <h6>見出し6</h6>
            <p>&nbsp;</p>
            <p>表組み</p>
            <table style="border-collapse: collapse; width: 100%;" border="1">
            <tbody>
            <tr>
            <td style="width: 50%;">
            <p>テキスト</p>
            </td>
            <td style="width: 50%;">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
            </tr>
            <tr>
            <td style="width: 50%;">テキストテキストテキスト</td>
            <td style="width: 50%;">テキストテキストテキスト</td>
            </tr>
            </tbody>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>左寄せのテキストです。<br />左寄せのテキストです。左寄せのテキストです。<br />左寄せのテキストです。</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">中央寄せのテキストです。<br />中央寄せのテキストです。中央寄せのテキストです。<br />中央寄せのテキストです。</p>
            <p>&nbsp;</p>
            <p style="text-align: right;">右寄せのテキストです。<br />右寄せのテキストです。右寄せのテキストです。<br />右寄せのテキストです。</p>
            <p>&nbsp;</p>
            <p style="text-align: justify;">両端揃えのテキストです。<br />両端揃えのテキストです。両端揃えのテキストです。<br />両端揃えのテキストです。</p>
            <p>&nbsp;</p>
            <ul>
            <li>箇条書きデフォルト</li>
            <li>箇条書きデフォルト</li>
            <li>箇条書きデフォルト</li>
            </ul>
            <ul style="list-style-type: circle;">
            <li>箇条書き 円</li>
            <li>箇条書き 円</li>
            <li>箇条書き 円</li>
            </ul>
            <ul style="list-style-type: disc;">
            <li>箇条書き 点</li>
            <li>箇条書き 点</li>
            <li>箇条書き 点</li>
            </ul>
            <ul style="list-style-type: square;">
            <li>箇条書き 四角</li>
            <li>箇条書き 四角</li>
            <li>箇条書き 四角</li>
            </ul>
            <ol>
            <li>箇条書き（数字 デフォルト）</li>
            <li>箇条書き（数字 デフォルト）</li>
            <li>箇条書き（数字 デフォルト）</li>
            </ol>
            <ol style="list-style-type: lower-alpha;">
            <li>箇条書き（数字 小文字アルファベット）</li>
            <li>箇条書き（数字 小文字アルファベット）</li>
            <li>箇条書き（数字 小文字アルファベット）</li>
            </ol>
            <ol style="list-style-type: lower-greek;">
            <li>箇条書き（数字 小文字ギリシャ文字）</li>
            <li>箇条書き（数字 小文字ギリシャ文字）</li>
            <li>箇条書き（数字 小文字ギリシャ文字）</li>
            </ol>
            <ol style="list-style-type: lower-roman;">
            <li>箇条書き（数字 小文字ローマ数字）</li>
            <li>箇条書き（数字 小文字ローマ数字）</li>
            <li>箇条書き（数字 小文字ローマ数字）</li>
            </ol>
            <ol style="list-style-type: upper-alpha;">
            <li>箇条書き（数字 大文字アルファベット）</li>
            <li>箇条書き（数字 大文字アルファベット）</li>
            <li>箇条書き（数字 大文字アルファベット）</li>
            </ol>
            <ol style="list-style-type: upper-roman;">
            <li>箇条書き（数字 大文字ローマ数字）</li>
            <li>箇条書き（数字 大文字ローマ数字）</li>
            <li>箇条書き（数字 大文字ローマ数字）</li>
            </ol>
            <p style="padding-left: 30px;">インデントが入るテキストインデントが入るテキスト<br />インデントが入るテキスト<br />インデントが入るテキスト</p>
            <p>&nbsp;</p>
            <p><a href="http://google.co.jp" target="_blank" rel="noopener">リンクの挿入</a></p>
            <p>&nbsp;</p>
            <p><img src="https://picsum.photos/500/500/?1" alt="画像" /></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

        </div>
        <?php /* ----------------- WYSIWYG用のラッパー ----------------- */ ?>


    </div>

    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/common-bottom-js.php';?>
    <?php /* ページごとのJS */ ?>
    <script>
        var wysiwygTable = function() {
        var table = $('.wysiwyg').find('table');
            table.wrap('<div class="table-wrap"></>');
        }
        wysiwygTable();
    </script>
    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-body-bottom.php';?>
</body>
</html>
<!DOCTYPE html>
