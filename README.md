# テンプレート

## 概要

本テンプレートはPHPの使用を前提として作成されています。  
また、WYSIWYGのスタイルを除いて、CSSやJSについては含んでいません。  
構築時に必要なHTMLの要素の記述漏れを防ぎ、サイト全体でPHPの変数等を設定することで作業効率と運用時のメンテナンスコストを下げることを目的に制作されました。

なお、タスクランナー等についても含めておらず、要件に合わせて適宜用意するようにしてください。

## ディレクトリ構成

**template直下**がドキュメントルートになります。

```
/template
├── .editorconfig //コーディングルール
├── README.md //本ドキュメント（使用時は削除）
├── assets //アセットファイル用のディレクトリ
│   ├── css
│   │   ├── helper.css
│   │   ├── demo-wysiwyg.css
│   │   └── wysiwyg.css
│   ├── images
│   ├── inc
│   │   ├── common-bottom-js.php
│   │   ├── common-css.php
│   │   ├── common-head-js.php
│   │   ├── config.php
│   │   ├── load-body-bottom.php
│   │   ├── load-body-top.php
│   │   ├── load-head-bottom.php
│   │   ├── load-head-top.php
│   │   └── meta.php
│   ├── js
│   ├── pdf //必要に応じて設置
│   └── font //WEBフォントがあれば使用
├── demo-wysiwyg.php //WYSIWYGのサンプル
└── page.php //テンプレートベースとなるページファイル
```

## コーディングルール

### 基本ルール

**.editorconfig**にページのコーディングルールを設定しています。  
使用方法については下記等を参考にしてください。  

[どんなエディタでもEditorConfigを使ってコードの統一性を高める \- Qiita](https://qiita.com/naru0504/items/82f09881abaf3f4dc171)

主な設定内容は下記の通りです。

|項目|設定値|
|----|----|
|文字コード|UTF-8|
|改行コード|LF|
|インデント種類|スペース|
|インデントサイズ|4| 

### HTML

- HTML上のパスは**ルートパス**で記載してください
- WYSIWYGに関してはdemo-wysiwyg.phpのサンプルソースを使用してください
- サイト内複数箇所で使用するようなコードはPHPのインクルードで共通ファイル化してください
- 各ページ内には**必ずh1を1つだけ**設定してください（トップページのみヘッダー内のロゴをh1に設定）
- [構造化データ](https://developers.google.com/search/docs/guides/intro-structured-data?hl=ja)についてはJSON-LDで設定してください

### アセットファイル

- アセットファイル及びインクルード用のファイルは全て**assets配下**へ格納してください。  
- assets/images配下のディレクトリ構造についてはサイトのコンテンツ等によってディレクトリを配置してください。後述のツリーは一例です。

```
│   ├── images
│   │   ├── common
│   │   ├── home
│   │   ├── event
│   │   ├── floorguide
│   │   └── access
```

### 画像

- 適切なサイズになるように書き出しを行ってください（素材をリサイズせずに使用しない）
- XDやPhotoshopで書き出した画像については[Tinypng](https://tinypng.com/)等で軽量化を行ってください

### JavaScript

- JavaScriptは基本的に`</body>`の直前に記述してください。
- jQueryなどのライブラリのみ`</head>`の直前に記述してください。
- jQueryを使用する場合は最新のものをCDNで読み込んでください。

### スタイルシート

- Sass等を使用して生成したCSSも前述のコーディングルールに従ってください
- リセットCSSに指定はありませんが、helper.cssおよびwysiwyg.css内のスタイルと後述する下記のスタイルはサイト全体で読み込むようにしてください

**全体に設定するスタイル**

```
body {
    -webkit-overflow-scrolling: touch;
    font-feature-settings: "palt";

    /* 状況に応じて入れる */
    -ms-word-break: break-all;
    word-break: break-all;
    /* 英語サイトは下記 */
    -ms-word-break: break-word;
    word-break: break-word;
}
```

**リセットCSSについて**  
迷った場合は下記参考にしてください。

■Eric Meyer’s “Reset CSS” 2.0  
https://meyerweb.com/eric/tools/css/reset/  
各要素をリセットしている部分に「box-sizing: border-box;」を追記すると使いやすいです。

■destyle.css  
https://github.com/nicolas-cusan/destyle.css/blob/master/destyle.css


## テンプレート

### ページファイル（page.php）

page.phpをベースにコーディングを開始してください。  
サイト全体で使用するものに関しては、後述するインクルードにてまとめて設定します。  

```
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/config.php';
```
上記は必須で読み込んでください。

```
//ページ設定
$page_title = '__TITLE__';
$page_description = '__DESCRIPTION__';
$page_keywords = '__KEYWORD__'; //不明な場合は値を空にしてください
$page_ogpimage = '__OGIMG__'; //OGP画像URL
```
meta.phpの中で各変数を読み込んでいます。  
詳細ページなどは、バックエンド側でDBから取ってきた値を出力します。

### インクルード

以下のインクルードで読み込むものに関しては、全てサイト全体で使用するものになります。  
あるページでしか使用しないjsやcssは記述しないようにしてください。

また、ヘッダー、フッター、サイドバーなどをまとめるインクルードは含めておりませんが、必要に応じて適宜追加作成してください。

|ファイル|説明|
|----|----|
|common-head-js.php| jQueryなどのライブラリの読み込み用 |
|common-bottom-js.php| スクリプトの読み込み用（common.jsなど）|
|common-css.php| スタイルの読み込み用（reset.css、common.cssなど）|
|config.php|サイト名などの基本情報の設定|
|load-head-top.php|`<head>` の上部の記述用（アナリティクス、各種広告タグなど）|
|load-head-bottom.php|`</head>` 直前の記述用（アナリティクス、各種広告タグなど）|
|load-body-top.php|`<body>` の直後の記述用（アナリティクス、各種広告タグなど）|
|load-body-bottom.php|`</body>` 直前の記述用（アナリティクス、各種広告タグなど）|
|meta.php|metaタグ、ファビコンなどの読み込み（各値についてはconfig.phpやページ側に設定した変数で設定）|

## WYSIWYGのスタイル

demo-wysiwyg.php

スタイルの設定がまちまちになりがちなWYSIWYGについてもスタイルを作成しました。  

### サンプルの使用方法

div.wysiwygごとそのままhtmlソースとして使用してください。  
また、wysiwyg.cssに関しては、中身のソースだけをコピーし、common.cssやlayout.cssなどのグローバルなcssファイルに記述するようにしてください。  
リンクのフォントカラー等の追加は可能です。

### wysiwyg内のtableに関して

tableに関してはスマホ閲覧時には画面をはみ出し、横スクロールをさせる想定です。  
横スクロールさせる際にtableをdivでラップさせる為jsを記述しています。  
このサンプルではファイル内の下部に記載していますが、実際にはcommon.jsなどのグローバルなJSファイルの中に記述してください。  
また、このサンプルのJSソースはあくまでサンプルとしています。  
div.wysiwyg内のtableをdiv.table-wrapでラップすることができれば変更していただいて構いません。

## 参考
[【企業サイト構築用】Webページコーディングガイドライン \- Qiita](https://qiita.com/kgsi/items/d8aa2424b86c0dc0d3f9)