<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/config.php';

//ページ設定
$fe_page_title = '__TITLE__';
$fe_page_description = '__DESCRIPTION__';
$fe_page_keywords = '__KEYWORD__'; //不明な場合は値を空にしてください
$fe_page_ogpimage = '__OGIMG__'; //OGP画像URL値を空にした場合はconfig.php内のデフォルトになる
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-head-top.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/meta.php';?>

<?php /* stylesheet */ ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/common-css.php';?>
<?php /* ページごとのCSS */ ?>
<!-- <link rel="stylesheet" href="/assets/css/home.css"> -->
<?php /* ページごとのCSS */ ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/common-head-js.php';?>

<script type="application/ld+json">
<?php /* ページごとのJSON-LDを記述 */ ?>
</script>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-head-bottom.php';?>
</head>
<body>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-body-top.php';?>

<p>コンテンツエリア</p>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/common-bottom-js.php';?>
<?php /* ページごとのJS */ ?>
<!-- <script src="/assets/js/home.js"></script> -->
<?php /* ページごとのJS */ ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/load-body-bottom.php';?>
</body>
</html>
